# Dentropy Cloud

## Service Directory

* default-gateway
  * ip: 10.133.0.1
* dentropy
  * ip: 10.133.0.3
* pihole
  * ip: 10.133.0.5
  * internal ports: 80
* wireguard
  * ip: 10.133.0.7
  * Ports: 51820
* private-nginx
  * ip: 10.133.0.10
  * internal ports: 80, 443
* public-nginx
  * ip: 10.133.0.9
  * internal ports: 80, 443
  * external ports: 80, 443
* gitlab
  * ip: 10.133.0.11
  * Ports: 22, 4443, 8080
* openproject
  * ip: 10.133.0.12
  * Ports: 8081
* trilium
  * ip: 10.133.0.13
  * internal ports: 8080
* nextcloud-postgres
  * ip: 10.133.0.14
  * internal ports: 5432
* nextcloud-web
  * ip: 10.133.0.15
  * internal ports: 80

### hosts to nginx proxy

``` conf
10.133.0.9    pihole.local      # Goes through private nginx proxy
10.133.0.11   gitlab.local
10.133.0.12   openproject.local
10.133.0.9    trilium.local     # Goes through private nginx proxy
10.133.0.15   nextcloud.local
```

## Installation instructions

All services need to be only accessable through wiregard until they are properally secured.

* Requirements
  * Linux machine preferably debian based with public static IP address
  * [ufw - uncomplicated firewall](https://en.wikipedia.org/wiki/Uncomplicated_Firewall)
    * built into ubuntu by default
  * git
  * docker that can be run as a user
  * docker-compose

**Copy these commands one at a time**

``` bash
ssh <user>@<ip address>
# Change ssh port to 23, find line that says "#Port 22" and change it to "Port 23"
sudo nano /etc/ssh/sshd_config
# Find line
sudo systemctl restart ssh
# Connection should break
ssh <user>@<ip address> -p 23
git clone https://gitlab.com/dentropy/DentropyCloud.git
cd DentropyCloud
bash setup.sh
# Wait for script to stop running
# Check if contianers are all up
docker ps -a

# exit to local machine
exit

# Copy wireguard certs to your local machine
scp -P 23 -r <user>@<ip address>:/srv/wireguard/* .

# Add a bunch of hosts to your hosts file, this will not work on windows machines
sudo su
echo "10.133.0.9    pihole.local      # Goes through private nginx proxy" >> /etc/hosts
echo "10.133.0.11   gitlab.local" >> /etc/hosts
echo "10.133.0.12   openproject.local" >> /etc/hosts
echo "10.133.0.9    trilium.local     # Goes through private nginx proxy" >> /etc/hosts
echo "10.133.0.15   nextcloud.local" >> /etc/hosts
cat /etc/hosts
```

## Proof of Concept

* Firewall
* Wireguard
* Nextcloud
* trilium
* Ghost
* Gitlab

## All Services

  - Gitlab
  - OpenProject
  - Nextcloud
  - trilium notes application
  - Syncthing
  - Browser Sync
      - Passwords - Bitwarden
      - Bookmarks - xBrowserSync
      - History - ActivityWatcher
  - Social Media
      - Pleroma / Mastadon
      - Matrix / Riot
      - Mattermost
      - Everything Fediverse
  - Capture internet trafic
      - http proxy interceptor
      - Wireguard or Nose to sniff off the kubernetes network
      - An analitics dashboard of what data was captured
  - Networking
      - openvpn
      - wireguard
      - A container to route traffic through different locations
        differently using iptables, also requires a gui
  - DNS
      - piHole
      - Bind 9
  - Blogs
      - Ghost
      - Wordpress
  - Wiki's
      - Wikimedia / Semantic Mediawiki
      - Wiki.js
      - DokuWiki
  - Video Conferencing
      - Jitsi
      - Nextcloud?
  - Read It Later / Pocket
      - Wallabag
  - Scheduling
      - Easy\! Appointments
  - Virtual Desktop
      - Apache guacamole
      - Run VM's on Kubernetes somehow

Other stuff to add includes photo editors, video editors, and games
## Nginx Port Config

All ports do not have to be external but they have to be part of the docker network so nginx can route to them though there are exceptions such as gitlab SSH

Do I want everything to have a hard coded IP address or continaer discovery. Manual IP address is easier.