cd gitlab && bash setup_gitlab.sh
cd ..
cd Nextcloud && bash setup_nextcloud.sh
cd ..
cd OpenProject && bash setup_OpenProject.sh
cd ..
cd pihole && bash setup_pihole.sh
cd ..
cd private-nginx && bash restart.sh
cd ..
cd wireguard && bash setup_wireguard.sh
cd ..
cd swag && bash restart.sh
cd ..
cd trilium && bash setup_trilium.sh