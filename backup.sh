#!/bin/bash

mkdir ~/DentropyCloud-backup
echo "Stopping OpenProject container"
cd OpenProject
docker-compose stop
cd ..
echo "Stopping pihole container"
cd pihole
docker-compose stop
cd ..
tar -cvf ~/DentropyCloud-backup/srv.tar /srv
cd OpenProject
echo "Restarting OpenProject container"
docker-compose start
cd ..
cd pihole
echo "Restarting pihole container"
docker-compose start
cd ..

echo "Stopping trilium container"
cd trilium
docker-compose stop
docker run -it --rm \
  -v trilium_trilium:/data \
  -v ~/DentropyCloud-backup:/backup ubuntu bash \
  -c "cd /data && tar cvf /backup/trilium_trilium.tar ."
echo "Starting trilium container"
docker-compose start
cd ..

echo "Stopping nextcloud containers"
cd Nextcloud
docker-compose stop
docker run --rm \
  -v nextcloud_nextcloud-postgres-db:/data \
  -v ~/DentropyCloud-backup:/backup ubuntu bash \
  -c "cd /data && tar cvf /backup/nextcloud_nextcloud-postgres-db.tar ."
docker run --rm \
  -v nextcloud_nextcloud-www:/data \
  -v ~/DentropyCloud-backup:/backup ubuntu bash \
  -c "cd /data && tar cvf /backup/nextcloud_nextcloud-www.tar ."
echo "Starting nextcloud containers"
docker-compose start
cd ~

echo "Creating meta tar ball"
tar -cvzf ~/DentropyCloud-backup-$(date +"%m_%d_%Y").tar.gz ./DentropyCloud-backup
sudo rm -r ./DentropyCloud-backup
echo "Backup complete"
