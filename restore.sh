#!/bin/bash

cd ~
tar -xvf DentropyCloud-backup*
cd DentropyCloud-backup
tar -xvf srv.tar -C /srv
docker volume create nextcloud_nextcloud-postgres-db
docker volume create nextcloud_nextcloud-www
docker volume create trilium_trilium
docker run --rm \
  -v nextcloud_nextcloud-postgres-db:/recover \
  -v ~/DentropyCloud-backup:/backup ubuntu bash \
  -c "cd /recover && tar xvf /backup/nextcloud_nextcloud-postgres-db.tar"
docker run --rm \
  -v nextcloud_nextcloud-www:/recover \
  -v ~/DentropyCloud-backup:/backup ubuntu bash \
  -c "cd /recover && tar xvf /backup/nextcloud_nextcloud-www.tar"
docker run --rm \
  -v trilium_trilium:/recover \
  -v ~/DentropyCloud-backup:/backup ubuntu bash \
  -c "cd /recover && tar xvf /backup/trilium_trilium.tar"
