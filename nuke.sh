#!/bin/bash

echo "Stopping all docker containers"
docker stop $(docker ps -aq)
echo "Removing all docker containers"
docker rm $(docker ps -aq)
echo "Removing /srv directory"
sudo rm -r /srv/*
docker volume prune