cd wireguard && docker-compose down
cd ..
cd trilium && docker-compose down
cd ..
cd pihole && docker-compose down
cd ..
cd Nextcloud && docker-compose down
cd ..
cd gitlab && bash docker-compose down
cd ..
cd OpenProject && docker-compose down
cd ..
cd private-nginx && docker-compose down
cd ..
cd swag && docker-compose down
cd ..
