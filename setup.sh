#!/bin/bash

sudo ufw allow 22
sudo ufw allow 23
sudo ufw allow 80
sudo ufw allow 443
sudo ufw allow 51820
sudo ufw enable

docker network create \
  --driver=bridge \
  --subnet=10.133.0.0/16 \
  --gateway=10.133.0.1 \
  dentropynet

./setup.sh